AWSTemplateFormatVersion: 2010-09-09
Description: Roles and Resources required to export Amazon WorkMail content programmatically

Resources:
  WorkMailExportBucket:
    Type: AWS::S3::Bucket

  WorkMailExportBucketKey:
    Type: AWS::KMS::Key
    DeletionPolicy: Retain
    Properties:
      Description: Symmetric CMK used to encrypt exported WorkMail
      KeyPolicy:
        Version: '2012-10-17'
        Id: workmail-export-key
        Statement:
        - Sid: Enable IAM User Permissions
          Effect: Allow
          Principal:
            AWS: !Sub 'arn:aws:iam::${AWS::AccountId}:root'
          Action: kms:*
          Resource: '*'
        - Sid: Allow administration of the key
          Effect: Allow
          Principal:
            AWS: !Sub 'arn:aws:iam::${AWS::AccountId}:role/Adminstrators'
            AWS: !Sub 'arn:aws:iam::${AWS::AccountId}:user/##########'
          Action:
            - kms:Create*
            - kms:Describe*
            - kms:Enable*
            - kms:List*
            - kms:Put*
            - kms:Update*
            - kms:Revoke*
            - kms:Disable*
            - kms:Get*
            - kms:Delete*
            - kms:ScheduleKeyDeletion
            - kms:CancelKeyDeletion
          Resource: '*'

  WorkMailExportRole:
    Type: AWS::IAM::Role
    Properties:
      Policies:
        - PolicyName: workmail-export
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: Allow
                Action:
                  - s3:AbortMultipartUpload
                  - s3:PutObject
                  - s3:GetBucketPolicyStatus
                Resource:
                  - !Sub 'arn:aws:s3:::${WorkMailExportBucket}'
                  - !Sub 'arn:aws:s3:::${WorkMailExportBucket}/*'
              - Effect: Allow
                Action:
                  - kms:Decrypt
                  - kms:GenerateDataKey
                Resource:
                  - !GetAtt WorkMailExportBucketKey.Arn
                Condition:
                  StringEquals:
                    kms:ViaService: !Sub 's3.${AWS::Region}.amazonaws.com'
                  StringLike:
                    kms:EncryptionContext:aws:s3:arn: !Sub 'arn:aws:s3:::${WorkMailExportBucket}/*'
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service: export.workmail.amazonaws.com
            Action: sts:AssumeRole
            Condition:
              StringEquals:
                sts:ExternalId: !Ref AWS::AccountId

Outputs:
  WorkMailBucket:
    Description: Bucket where Amazon WorkMail emails are exported to
    Value: !Ref WorkMailExportBucket
  
  WorkMailKMSKeyArn:
    Description: ARN of the KMS key used to encrypt exported WorkMail
    Value: !GetAtt WorkMailExportBucketKey.Arn

  WorkMailRoleArn:
    Description: ARN of the IAM role used to export WorkMail
    Value: !GetAtt WorkMailExportRole.Arn
