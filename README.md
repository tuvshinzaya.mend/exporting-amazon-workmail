**Prerequisites**

1. [Required] AWS Account login with a User or Role that you login with.
 If you don't know what I mean about role, then you probably use a User account!

2. [Required] Amazon WorkMail setup that you plan to export.
 You will need AWS Console access to the WorkMail organisation, and the method outlined in this post can't be done if you only have access to the mailbox through webmail.

3. [Required] AWS CLI setup and configured. Refer to this handy guide to do it.



> **Step 1: WorkMail Export Roles**
Grab a copy of the following CloudFormation template either from below, or from the Git Gist here and save it to a file called 
> template.yml.

Open up the **~~template.yml~~** file in your favourite text/code editor as we're going to be making a small change to it to that it's suitable for your usecase.

Uncomment one of the following two lines and replace **~~REPLACE_ROLE_NAME_HERE~~** or **~~REPLACE_USERNAME_HERE~~** with either:

The username of your user
The role you use to login with AWS with.
```
# AWS: !Sub 'arn:aws:iam::${AWS::AccountId}:role/REPLACE_ROLE_NAME_HERE'
# AWS: !Sub 'arn:aws:iam::${AWS::AccountId}:user/REPLACE_USERNAME_HERE'
```

**With the template correctly edited, open up a terminal with access to the aws cli, and run the following command to deploy the template.**
> Note, you should deploy this template into the same region where you Amazon WorkMail is deployed. Head to the WorkMail console to verify where you organisation is deployed: https://console.aws.amazon.com/workmail/v2/home. In my case it's us-east-1.
```
aws cloudformation deploy \
  --template-file template.yml \
  --stack-name workmail-export \
  --region us-east-1 \
  --capabilities CAPABILITY_NAMED_IAM
```
> Once the deploy has completed, retrieve the output values from the CloudFormation stack by running the following:

aws cloudformation describe-stacks \
  --stack-name workmail-export \
  --region us-east-1 \
  --query "Stacks[0].Outputs"
```
# [
#   {
#     "OutputKey": "WorkMailBucket",
#     "OutputValue": "workmail-export-workmailexportbucket-1235qxlijj2mt",
#     "Description": "Bucket where Amazon WorkMail emails are exported to"
#   },
#   {
#     "OutputKey": "WorkMailRoleArn",
#     "OutputValue": "arn:aws:iam::01234567890:role/workmail-export-WorkMailExportRole-WRZX3S4WQPZP",
#     "Description": "ARN of the IAM role used to export WorkMail"
#   },
#   {
#     "OutputKey": "WorkMailKMSKeyArn",
#     "OutputValue": "arn:aws:kms:us-east-1:01234567890:key/xxxxxxxx-xxxx-xxxx-xxxx-ce62c595ffba",
#     "Description": "ARN of the KMS key used to encrypt exported WorkMail"
#   }
# ]
```

> **Step 2: Running WorkMail Export**
To run the WorkMail export, the following command should be run in a terminal with access to the **aws cli**.

Replace the values in each **export** statement with the outputs from the previous step (the bucket name, the KMS key ARN and the IAM role ARN).

```
export WorkMailExportBucketKey_arn="arn:aws:kms:us-east-1:01234567890:key/xxxxxxxx-xxxx-xxxx-xxxx-ce62c595ffba"
export WorkMailExportRole_arn="arn:aws:iam::01234567890:role/workmail-export-WorkMailExportRole-WRZX3S4WQPZP"
export WorkMailExportBucket_name="workmail-export-workmailexportbucket-1235qxlijj2mt"
```

Then navigate to the Amazon WorkMail console and retrieve your Organisation ID.
And the User ID of the mailbox you want to export.

```
export WORKMAIL_ORG_ID="m-695xxxxxxxxxxxxxxxxxxxxxxxxxxx"
export WORKMAIL_USER_ID="54640044-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
```

```
aws workmail start-mailbox-export-job \
  --organization-id $WORKMAIL_ORG_ID \
  --entity-id $WORKMAIL_USER_ID \
  --kms-key-arn $WorkMailExportBucketKey_arn \
  --role-arn $WorkMailExportRole_arn \
  --s3-bucket-name $WorkMailExportBucket_name \
  --s3-prefix export
```

You can check the status of the job by running the commands below (passing the Job ID from the first command to the second):

```
aws workmail list-mailbox-export-jobs \
  --organization-id $WORKMAIL_ORG_ID

aws workmail describe-mailbox-export-job \
  --organization-id $WORKMAIL_ORG_ID \
  --job-id $JOB_ID_FROM_ABOVE
```

> **Step 3: Download the Email Files**
The final step is the easiest, you just need to navigate to the S3 console and download the files from the newly created bucket (the one specified in the WorkMailBucket output from the CloudFomration stack)





